import { Component, OnInit, ViewChild } from '@angular/core';
import { OauthService } from '../core/oauth.service';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'sds-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @ViewChild('form', {static: false})
  form: NgForm | undefined;

  private subscription: Subscription | undefined;

  constructor(
    protected oauthService: OauthService) {
  }

  ngOnInit(): void {
  }

}
