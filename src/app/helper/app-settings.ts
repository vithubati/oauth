import { environment } from '../../environments/environment';

export class APIEndpoints {
  public static SERVER_API = environment.APP_DOMAIN_NAME;
  //
  public static GITHUB_OAUTH_API = 'api/github/oauth';
}
